var express         = require('express');
var router          = express.Router();
var UserController  = require('../controllers/controllersuserapi');
var verifyToken     = require('../api/routes/auth');
var upload          = require("../controllers/upload");

router.get('/api/getallUsers', verifyToken, UserController.getallUsers);
router.get('/api/getallclaims', verifyToken,UserController.getallclaims);
router.get('/api/getclaimdetails/:id',verifyToken,UserController.getclaimdetails);
router.post('/api/createUser', verifyToken,verifyToken, UserController.createUser);
//router.post('/api/uploadfile', verifyToken,upload.single("file"), UserController.uploadfile);
router.post('/api/edit/:id',   verifyToken, UserController.updateUsebyId);
router.get('/api/user/:id',    verifyToken, UserController.getUsebyId);
router.delete('/api/user/:id', verifyToken, UserController.deleteUsebyId);
router.post('/api/login',      UserController.login);

module.exports=router;