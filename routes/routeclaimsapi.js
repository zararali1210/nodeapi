var express         = require('express');
var router          = express.Router();
var UserController  = require('../controllers/controllerclaimsapi');
var ClaimMasterController  = require('../controllers/controllerclaimmasterapi');
var verifyToken     = require('../api/routes/auth');
var upload          = require("../controllers/upload");

router.get('/api/getallarcData', verifyToken, UserController.getallarcData);
router.get('/api/getallPickupData', verifyToken, UserController.getallPickupData);
router.get('/api/getallDispatchData', verifyToken, UserController.getallDispatchData);
router.post('/api/updateClaimMaster/:id',verifyToken,UserController.updateClaimMaster, UserController.createClaimHistory);
router.get('/api/getClaimStatus', verifyToken, UserController.getClaimStatus);
router.get('/api/getClaimSubStatus/:id',verifyToken,UserController.getClaimSubStatus);
//router.post('/api/createClaimHistory',UserController.createClaimHistory);
router.post('/api/uploadfile',verifyToken,upload.single("file"),ClaimMasterController.uploadfile);


module.exports=router;