var {
    sequelize
} = require('../models');
var {
    ARC: arcModel
} = require('../models');
var {
    Dispatch: dispatchModel
} = require('../models');
var {
    ClaimStatus: claimStatusModel,
    Pickup: pickupModel,
    ClaimHistory: updatedClaimDataModel,
    ClaimMaster: claimMasterModel
} = require('../models');

module.exports.getallarcData = function (req, res) {
    arcModel.findAll({}).then(data => {
        if (data.length > 0) {
            res.json({
                code: 200,
                status: true,
                response: data
            });
        } else {
            res.json({
                status: false,
                message: "Records Not found",
                response: null
            });
        }
    }).catch(er => {
        res.json({
            status: er
        });
    });
}

module.exports.getallPickupData = function (req, res) {
    dispatchModel.findAll({}).then(data => {
        if (data.length > 0) {
            res.json({
                code: 200,
                status: true,
                response: data
            });
        } else {
            res.json({
                status: false,
                message: "Records Not found",
                response: null
            });
        }
    }).catch(er => {
        res.json({
            status: er
        });
    });
}

module.exports.getallDispatchData = function (req, res) {
    pickupModel.findAll({}).then(data => {
        if (data.length > 0) {
            res.json({
                code: 200,
                status: true,
                response: data
            });
        } else {
            res.json({
                status: false,
                message: "Records Not found",
                response: null
            });
        }
    }).catch(er => {
        res.json({
            status: er
        });
    });
}

module.exports.getClaimStatus = function (req, res) {
    claimStatusModel.findAll({}).then(data => {
        if (data.length > 0) {
            res.json({
                code: 200,
                status: true,
                response: data
            });
        } else {
            res.json({
                status: false,
                message: "Records Not found",
                response: null
            });
        }
    }).catch(er => {
        res.json({
            status: er
        });
    });
}




module.exports.getClaimSubStatus = function (req, res) {

    sequelize.query("select * from claim_substatuses where claim_status_id ='" + req.params.id + "'", {
        type: sequelize.QueryTypes.SELECT
    }).then(data => {
        if (data.length > 0) {
            res.json({
                code: 200,
                status: true,
                data: data
            });
        } else {
            res.json({
                status: false,
                message: "Record Not Found",
                response: null
            });
        }
    }).catch(er => {
        res.json({
            status: er
        });
    });
}

module.exports.updateClaimMaster = function (req, res, next) {

    let body = {
                'claims_claims_number'  : req.body.claims_claims_number,
                'updated_by'            : req.body.updated_by,
                'pickup'                : req.body.pickup,
                'dispatch'              : req.body.dispatch,
                'arc'                   : req.body.arc,
                'comments'              : req.body.comments,
                'claim_status'          : req.body.claim_status,
                'claim_sub_status'      : req.body.claim_sub_status,
        }

    if (body['pickup'].length > 0 || body['dispatch'].length > 0 || body['arc'].length > 0) {
        let body = {
            'claims_claims_number'  : req.body.claims_claims_number,
            'updated_by'            : req.body.updated_by,
            'pickup'                : req.body.pickup,
            'dispatch'              : req.body.dispatch,
            'arc'                   : req.body.arc
        }
         claimMasterModel.update(body, {
            where: {
                "claims_claims_number": req.params.id
            }
        }).then(data => {
            if (data > 0) {
                console.log('Done Response');
            }
           // next();
        }).catch(er => {
            res.json({
                status: er
            });
        });
    } else {
        console.log('***');
        //next();
    }
    if (body['claim_sub_status'].length > 0 || body['claim_status'].length > 0 || body['comments'].length > 0) {
        let body = {
            'claims_claims_number'   : req.body.claims_claims_number,
            'updated_by'             : req.body.updated_by,
            'claim_status'           : req.body.claim_status,
            'claim_sub_status'       : req.body.claim_sub_status,
            'comments'               : req.body.comments
         }
        
         claimMasterModel.update(body, {
            where: {
                "claims_claims_number" : req.params.id
            }
        }).then(data => {
            if (data > 0) {
                console.log('Done Response');
            }
            next();
        }).catch(er => {
            res.json({
                status: er
            });
        });
    } else {
        next();
   }
}


module.exports.createClaimHistory = function (req, res) {

    let body = {
        'claims_claims_number'   : req.body.claims_claims_number,
        'pickup'                 : req.body.pickup,
        'dispatch'               : req.body.dispatch,
        'arc'                    : req.body.arc,
        'claim_status'           : req.body.claim_status,
        'claim_sub_status'       : req.body.claim_sub_status,
        'updated_by'             : req.body.updated_by,
        'comments'               : req.body.comments
    }
  
    updatedClaimDataModel.create(body).then(user => {
        res.json({
            code: 200,
            message: "Updated Successfully",
            status: true
        });
    }).catch(er => {
        res.json({
            status: er
        });
    });
}