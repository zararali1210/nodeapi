const readXlsxFile           = require('read-excel-file/node');
const fs                     = require('fs')
const fastcsv                = require("fast-csv");
var path                     = require('path');
var { getJsDateFromExcel } = require("excel-date-to-js");
var dateFormat = require('dateformat');
global.__basedir             = __dirname;

function reverseDate(input){
    if(input != null || input !== undefined || input !== ''){
        var str = input.split('-');
        var value = str[2]+'-'+str[1]+'-'+str[0];
        return value;
    }
}


/*************Upload file************/
module.exports.uploadfile = function (req, res) {
   
    var mysql = require('mysql');
    var conn = mysql.createConnection({
        host     : 'localhost',
        user     : 'nodeapi',
        password : 'nodeapi',
        database : 'nodeapi'
    });

    var ext = path.extname(req.file.originalname);
    if (ext !== ".xlsx") {
        fs.unlinkSync(req.file.path)
        res.json({
            status: false,
            "msg": "Only xlsx Allowed"
        })
    }
    if (ext == ".xlsx") {
        readXlsxFile(req.file.path).then((csvData) => {

            var  excelHeader = [
                'Claims Claims No',
                'Claims IMEI',
                'Claims Policy Start Date',
                'Claims Policy End Date',
                'Claims Mobile Purchase Date',
                'Claims Store Name',
                'Claims Policy Type',
                'Claims Master Policy Number',
                'Claims Certificate of Insurance',
                'Claims Make',
                'Claims Model',
                'Claims Cost - Mobile',
                'Claims Policy Status',
                'Claims Created Time',
                'Claims Modified Time',
                'Claims Last Modified By',
                'Claims Client Name',
                'Claims Customer Name',
                'Claims Home Contact',
                'Claims Email',
                'Claims Alternate Landline No',
                'Claims Subject',
                'Claims Claim Date',
                'Claims Claim Type',
                'Claims Assigned To',
                'Claims Claim Cause',
                'Claims Coverage Type',
                'Claims Contact Number',
                'Claims Date of Incident',
                'Claims Where Did It Happen',
                'Claims Time of Incident',
                'Claims Delay in Claim Intimation',
                'Claims Pickup Company',
                'Claims Pickup Street / Area',
                'Claims Unit # / Floor / House no. / Block / Landmark',
                'Claims Pickup City',
                'Claims Pickup State',
                'Claims Pin Code',
                'Claims Pickup Country',
                'Claims Dispatch Company',
                'Claims Dispatch Street / Area',
                'Claims Dispatch Unit # / Floor / House no. / Block',
                'Claims Dispatch City',
                'Claims Dispatch State',
                'Claims Dispatch Pin code',
                'Claims Dispatch Country',
                'Claims Excess paid Date',
                'Claims Excess Fee (Rs)',
                'Claims Excess Paid by Customer',
                'Claims UTR / Cheque Number',
                'Claims Excess Fee Actual',
                'Claims Excess Fee Status',
                'Claims Excess Fee BER',
                'Claims Excess Fee BER Type',
                'Claims Excess Fee BER Status',
                'Claims Excess Fee BER Actual',
                'Claims Policy Aging'
              ];
            var is_same = excelHeader.length == csvData[0].length && excelHeader.every(function(element, index) {
            var x = csvData[0][index].toLowerCase();
            csvData[0][index] = x;
            return element.toLowerCase() == csvData[0][index]; 
            })
            
          if (is_same == true) {
                conn.connect(error => {
                    if (error) {
                       // console.error(error);
                    } else {
                       csvData.shift();
                       for (i = 0; i < csvData.length; i++) {
                       if(csvData[i][3] != null && csvData[i][3] !== undefined && csvData[i][3] !== '-' && csvData[i][3] !== '')
                      {
                        csvData[i][3] =dateFormat(getJsDateFromExcel(csvData[i][3]), "yyyy-mm-dd");
                      }
                      if(csvData[i][4] != null && csvData[i][4] !== undefined && csvData[i][4] !== '-' && csvData[i][4] !== '')
                      {
                          csvData[i][4]     =  dateFormat(getJsDateFromExcel(csvData[i][4]), "yyyy-mm-dd");
                      }
                      if(csvData[i][2] != null && csvData[i][2] !== undefined && csvData[i][2] !== '-' && csvData[i][2] !== '')
                      {
                          csvData[i][2]     =  dateFormat(getJsDateFromExcel(csvData[i][2]), "yyyy-mm-dd");
                      }

                      if(csvData[i][22] != null && csvData[i][22] !== undefined && csvData[i][22] !== '-' && csvData[i][22] !== '')
                      {
                          csvData[i][22]     =  dateFormat(getJsDateFromExcel(csvData[i][22]), "yyyy-mm-dd");
                      }
                      if(csvData[i][28] != null && csvData[i][22] !== undefined && csvData[i][28] !== '-' && csvData[i][28] !== '')
                      {
                          csvData[i][28]     =  dateFormat(getJsDateFromExcel(csvData[i][28]), "yyyy-mm-dd");
                      }
                      if(csvData[i][46] != null && csvData[i][46] !== undefined && csvData[i][46] !== '-' && csvData[i][46] !== '')
                      {
                          csvData[i][46]     =  dateFormat(getJsDateFromExcel(csvData[i][46]), "yyyy-mm-dd");
                      }
                      var updateQtyQuery = "INSERT INTO  claim_masters (claims_claims_number, claims_imei, claims_policy_start_date, claims_policy_end_date, claims_mobile_purchase_date, claims_store_name, claims_policy_type, claims_master_policy_number, claims_certificate_of_insurance, claims_make, claims_model, claims_cost_mobile, claims_policy_status, claims_created_time, claims_modified_time, claims_last_modified_by, claims_client_name, claims_customer_name, claims_home_contact, claims_email, claims_alternative_landline_number, claims_subject, claims_claim_date, claims_claim_type, claims_assigned_to, claims_claim_cause, claims_coverage_type, claims_contact_number, claims_date_of_incident, claims_where_did_it_happen, claims_time_of_incident, claims_delay_in_claim_information, claims_pickup_company, claims_pickup_street_area, claims_unit, claims_pick_up_city, claims_pick_up_state, claims_pin_code, claims_pickup_country, claims_dispatch_company, claims_dispatch_street_area, claims_dispatch_unit, claims_dispatch_city, claims_dispatch_state, claims_dispatch_pincode, claims_dispatch_country, claims_excess_paid_date, claims_excess_fee, claims_excess_paid_by_customer, claims_utr_cheque_number, claims_excess_fee_actual, claims_excess_fee_status, claims_excess_fee_ber, claims_excess_fee_ber_type, claims_excess_fee_ber_status, claims_excess_fee_ber_actual, claims_policy_aging) VALUES('" 
                      + csvData[i][0] + "', '"
                      + csvData[i][1] + "', '"
                      + csvData[i][2] + "', '"
                      + csvData[i][3] + "', '"
                      + csvData[i][4] + "', '"
                      + csvData[i][5] + "', '"
                      + csvData[i][6] + "', '"
                      + csvData[i][7] + "', '"
                      + csvData[i][8] + "', '"
                      + csvData[i][9] + "', '"
                      + csvData[i][10] + "', '"
                      + csvData[i][11] + "', '"
                      + csvData[i][12] + "', '"
                      + csvData[i][13] + "', '"
                      + csvData[i][14] + "', '"
                      + csvData[i][15] + "', '"
                      + csvData[i][16] + "', '"
                      + csvData[i][17] + "', '"
                      + csvData[i][18] + "', '"
                      + csvData[i][19] + "', '"
                      + csvData[i][20] + "', '"
                      + csvData[i][21] + "', '"
                      + csvData[i][22] + "', '"
                      + csvData[i][23] + "', '"
                      + csvData[i][24] + "', '"
                      + csvData[i][25] + "', '"
                      + csvData[i][26] + "', '"
                      + csvData[i][27] + "', '"
                      + csvData[i][28] + "', '"
                      + csvData[i][29] + "', '"
                      + csvData[i][30] + "', '"
                      + csvData[i][31] + "', '"
                      + csvData[i][32] + "', '"
                      + csvData[i][33] + "', '"
                      + csvData[i][34] + "', '"
                      + csvData[i][35] + "', '"
                      + csvData[i][36] + "', '"
                      + csvData[i][37] + "', '"
                      + csvData[i][38] + "', '"
                      + csvData[i][39] + "', '"
                      + csvData[i][40] + "', '"
                      + csvData[i][41] + "', '"
                      + csvData[i][42] + "', '"
                      + csvData[i][43] + "', '"
                      + csvData[i][44] + "', '"
                      + csvData[i][45] + "', '"
                      + csvData[i][46] + "', '"
                      + csvData[i][47] + "', '"
                      + csvData[i][48] + "', '"
                      + csvData[i][49] + "', '"
                      + csvData[i][40] + "', '"
                      + csvData[i][51] + "', '"
                      + csvData[i][52] + "', '"
                      + csvData[i][53] + "', '"
                      + csvData[i][54] + "', '"
                      + csvData[i][55] + "', '"
                      + csvData[i][56] + "') ON DUPLICATE KEY UPDATE claims_claims_number='"
                      + csvData[i][0] + "',claims_imei='" 
                      + csvData[i][1] + "',claims_policy_start_date='" 
                      + csvData[i][2] + "',claims_policy_end_date='" 
                      + csvData[i][3] + "',claims_mobile_purchase_date='" 
                      + csvData[i][4] + "',claims_store_name='" 
                      + csvData[i][5] + "',claims_policy_type='" 
                      + csvData[i][6] + "',claims_master_policy_number='" 
                      + csvData[i][7] + "',claims_certificate_of_insurance='" 
                      + csvData[i][8] + "',claims_make='" 
                      + csvData[i][9] + "',claims_model='" 
                      + csvData[i][10] + "',claims_cost_mobile='" 
                      + csvData[i][11] + "',claims_policy_status='" 
                      + csvData[i][12] + "',claims_created_time='" 
                      + csvData[i][13] + "',claims_modified_time='" 
                      + csvData[i][14] + "',claims_last_modified_by='" 
                      + csvData[i][15] + "',claims_client_name='" 
                      + csvData[i][16] + "',claims_customer_name='" 
                      + csvData[i][17] + "',claims_home_contact='" 
                      + csvData[i][18] + "',claims_email='" 
                      + csvData[i][19] + "',claims_alternative_landline_number='" 
                      + csvData[i][20] + "',claims_subject='" 
                      + csvData[i][21] + "',claims_claim_date='" 
                      + csvData[i][22] + "',claims_claim_type='" 
                      + csvData[i][23] + "',claims_assigned_to='" 
                      + csvData[i][24] + "',claims_claim_cause='" 
                      + csvData[i][25] + "',claims_coverage_type='" 
                      + csvData[i][26] + "',claims_contact_number='" 
                      + csvData[i][27] + "',claims_date_of_incident='" 
                      + csvData[i][28] + "',claims_where_did_it_happen='" 
                      + csvData[i][29] + "',claims_time_of_incident='" 
                      + csvData[i][30] + "',claims_delay_in_claim_information='" 
                      + csvData[i][31] + "',claims_pickup_company='" 
                      + csvData[i][32] + "',claims_pickup_street_area='" 
                      + csvData[i][33] + "',claims_unit='" 
                      + csvData[i][34] + "',claims_pick_up_city='" 
                      + csvData[i][35] + "',claims_pick_up_state='" 
                      + csvData[i][36] + "',claims_pin_code='" 
                      + csvData[i][37] + "',claims_pickup_country='" 
                      + csvData[i][38] + "',claims_dispatch_company='" 
                      + csvData[i][39] + "',claims_dispatch_street_area='" 
                      + csvData[i][40] + "',claims_dispatch_unit='" 
                      + csvData[i][41] + "',claims_dispatch_city='" 
                      + csvData[i][42] + "',claims_dispatch_state='" 
                      + csvData[i][43] + "',claims_dispatch_pincode='" 
                      + csvData[i][44] + "',claims_dispatch_country='" 
                      + csvData[i][45] + "',claims_excess_paid_date='" 
                      + csvData[i][46] + "',claims_excess_fee='" 
                      + csvData[i][47] + "',claims_excess_paid_by_customer='" 
                      + csvData[i][48] + "',claims_utr_cheque_number='" 
                      + csvData[i][49] + "',claims_excess_fee_actual='" 
                      + csvData[i][50] + "',claims_excess_fee_status='" 
                      + csvData[i][51] + "',claims_excess_fee_ber='" 
                      + csvData[i][52] + "',claims_excess_fee_ber_type='"
                      + csvData[i][53] + "',claims_excess_fee_ber_status='" 
                      + csvData[i][54] + "',claims_excess_fee_ber_actual='" 
                      + csvData[i][55] + "',claims_policy_aging='"
                      + csvData[i][56] + "';";
                       conn.query(updateQtyQuery,[csvData],(error, results, fields) => {
                      try {
                        res.json({
                           message: "File Uploaded Successfully",
                           code: 200,
                           status: 'true'
                        });
                       }
                       catch(err) {
                           // console.log('Upload File:',err);
                       }
                     });
                    }}
                });
            } else {
                fs.unlinkSync(req.file.path)
                res.json({
                    status: false,
                    message: "Column(s) does not match."
                });
            }
        })
       
    }

    if (ext == ".csv") {
        let stream = fs.createReadStream(req.file.path);
        let csvData = [];
        let csvStream = fastcsv
            .parse()
            .on("data", function (data) {
                csvData.push(data);
            })
            .on("end", function () {
                var  excelHeader = [
                    'Claims Claims No',
                    'Claims IMEI',
                    'Claims Policy Start Date',
                    'Claims Policy End Date',
                    'Claims Mobile Purchase Date',
                    'Claims Store Name',
                    'Claims Policy Type',
                    'Claims Master Policy Number',
                    'Claims Certificate of Insurance',
                    'Claims Make',
                    'Claims Model',
                    'Claims Cost - Mobile',
                    'Claims Policy Status',
                    'Claims Created Time',
                    'Claims Modified Time',
                    'Claims Last Modified By',
                    'Claims Client Name',
                    'Claims Customer Name',
                    'Claims Home Contact',
                    'Claims Email',
                    'Claims Alternate Landline No',
                    'Claims Subject',
                    'Claims Claim Date',
                    'Claims Claim Type',
                    'Claims Assigned To',
                    'Claims Claim Cause',
                    'Claims Coverage Type',
                    'Claims Contact Number',
                    'Claims Date of Incident',
                    'Claims Where Did It Happen',
                    'Claims Time of Incident',
                    'Claims Delay in Claim Intimation',
                    'Claims Pickup Company',
                    'Claims Pickup Street / Area',
                    'Claims Unit # / Floor / House no. / Block / Landmark',
                    'Claims Pickup City',
                    'Claims Pickup State',
                    'Claims Pin Code',
                    'Claims Pickup Country',
                    'Claims Dispatch Company',
                    'Claims Dispatch Street / Area',
                    'Claims Dispatch Unit # / Floor / House no. / Block',
                    'Claims Dispatch City',
                    'Claims Dispatch State',
                    'Claims Dispatch Pin code',
                    'Claims Dispatch Country',
                    'Claims Excess paid Date',
                    'Claims Excess Fee (Rs)',
                    'Claims Excess Paid by Customer',
                    'Claims UTR / Cheque Number',
                    'Claims Excess Fee Actual',
                    'Claims Excess Fee Status',
                    'Claims Excess Fee BER',
                    'Claims Excess Fee BER Type',
                    'Claims Excess Fee BER Status',
                    'Claims Excess Fee BER Actual',
                    'Claims Policy Aging'
                  ];

                 
                var is_same = excelHeader.length == csvData[0].length && excelHeader.every(function (element, index) {
                    var x = csvData[0][index].toLowerCase();
                    csvData[0][index] = x;
                    return element.toLowerCase() == csvData[0][index];
                });
               if (is_same == true) {

                    conn.connect(error => {
                        if (error) {
                           // console.error(error);
                        } else {
                           csvData.shift();
                           var { getJsDateFromExcel } = require("excel-date-to-js");
                           var dateFormat = require('dateformat');
                           for (i = 0; i < csvData.length; i++) {
                           var updateQtyQuery = "INSERT INTO claim_masters ( claims_claims_number, claims_imei, claims_policy_start_date, claims_policy_end_date, claims_mobile_purchase_date, claims_store_name, claims_policy_type, claims_master_policy_number, claims_certificate_of_insurance, claims_make, claims_model, claims_cost_mobile, claims_policy_status, claims_created_time, claims_modified_time, claims_last_modified_by, claims_client_name, claims_customer_name, claims_home_contact, claims_email, claims_alternative_landline_number, claims_subject, claims_claim_date, claims_claim_type, claims_assigned_to, claims_claim_cause, claims_coverage_type, claims_contact_number, claims_date_of_incident, claims_where_did_it_happen, claims_time_of_incident, claims_delay_in_claim_information, claims_pickup_company, claims_pickup_street_area, claims_unit, claims_pick_up_city, claims_pick_up_state, claims_pin_code, claims_pickup_country, claims_dispatch_company, claims_dispatch_street_area, claims_dispatch_unit, claims_dispatch_city, claims_dispatch_state, claims_dispatch_pincode, claims_dispatch_country, claims_excess_paid_date, claims_excess_fee, claims_excess_paid_by_customer, claims_utr_cheque_number, claims_excess_fee_actual, claims_excess_fee_status, claims_excess_fee_ber, claims_excess_fee_ber_type, claims_excess_fee_ber_status, claims_excess_fee_ber_actual, claims_policy_aging) VALUES('" 
                          + csvData[i][0] + "', '"
                          + csvData[i][1] + "', '"
                          + csvData[i][2] + "', '"
                          + csvData[i][3] + "', '"
                          + csvData[i][4] + "', '"
                          + csvData[i][5] + "', '"
                          + csvData[i][6] + "', '"
                          + csvData[i][7] + "', '"
                          + csvData[i][8] + "', '"
                          + csvData[i][9] + "', '"
                          + csvData[i][10] + "', '"
                          + csvData[i][11] + "', '"
                          + csvData[i][12] + "', '"
                          + csvData[i][13] + "', '"
                          + csvData[i][14] + "', '"
                          + csvData[i][15] + "', '"
                          + csvData[i][16] + "', '"
                          + csvData[i][17] + "', '"
                          + csvData[i][18] + "', '"
                          + csvData[i][19] + "', '"
                          + csvData[i][20] + "', '"
                          + csvData[i][21] + "', '"
                          + csvData[i][22] + "', '"
                          + csvData[i][23] + "', '"
                          + csvData[i][24] + "', '"
                          + csvData[i][25] + "', '"
                          + csvData[i][26] + "', '"
                          + csvData[i][27] + "', '"
                          + csvData[i][28] + "', '"
                          + csvData[i][29] + "', '"
                          + csvData[i][30] + "', '"
                          + csvData[i][31] + "', '"
                          + csvData[i][32] + "', '"
                          + csvData[i][33] + "', '"
                          + csvData[i][34] + "', '"
                          + csvData[i][35] + "', '"
                          + csvData[i][36] + "', '"
                          + csvData[i][37] + "', '"
                          + csvData[i][38] + "', '"
                          + csvData[i][39] + "', '"
                          + csvData[i][40] + "', '"
                          + csvData[i][41] + "', '"
                          + csvData[i][42] + "', '"
                          + csvData[i][43] + "', '"
                          + csvData[i][44] + "', '"
                          + csvData[i][45] + "', '"
                          + csvData[i][46] + "', '"
                          + csvData[i][47] + "', '"
                          + csvData[i][48] + "', '"
                          + csvData[i][49] + "', '"
                          + csvData[i][40] + "', '"
                          + csvData[i][51] + "', '"
                          + csvData[i][52] + "', '"
                          + csvData[i][53] + "', '"
                          + csvData[i][54] + "', '"
                          + csvData[i][55] + "', '"
                          + csvData[i][56] + "') ON DUPLICATE KEY UPDATE claims_claims_number='"
                          + csvData[i][0] + "',claims_imei='" 
                          + csvData[i][1] + "',claims_policy_start_date='" 
                          + csvData[i][2] + "',claims_policy_end_date='" 
                          + csvData[i][3] + "',claims_mobile_purchase_date='" 
                          + csvData[i][4] + "',claims_store_name='" 
                          + csvData[i][5] + "',claims_policy_type='" 
                          + csvData[i][6] + "',claims_master_policy_number='" 
                          + csvData[i][7] + "',claims_certificate_of_insurance='" 
                          + csvData[i][8] + "',claims_make='" 
                          + csvData[i][9] + "',claims_model='" 
                          + csvData[i][10] + "',claims_cost_mobile='" 
                          + csvData[i][11] + "',claims_policy_status='" 
                          + csvData[i][12] + "',claims_created_time='" 
                          + csvData[i][13] + "',claims_modified_time='" 
                          + csvData[i][14] + "',claims_last_modified_by='" 
                          + csvData[i][15] + "',claims_client_name='" 
                          + csvData[i][16] + "',claims_customer_name='" 
                          + csvData[i][17] + "',claims_home_contact='" 
                          + csvData[i][18] + "',claims_email='" 
                          + csvData[i][19] + "',claims_alternative_landline_number='" 
                          + csvData[i][20] + "',claims_subject='" 
                          + csvData[i][21] + "',claims_claim_date='" 
                          + csvData[i][22] + "',claims_claim_type='" 
                          + csvData[i][23] + "',claims_assigned_to='" 
                          + csvData[i][24] + "',claims_claim_cause='" 
                          + csvData[i][25] + "',claims_coverage_type='" 
                          + csvData[i][26] + "',claims_contact_number='" 
                          + csvData[i][27] + "',claims_date_of_incident='" 
                          + csvData[i][28] + "',claims_where_did_it_happen='" 
                          + csvData[i][29] + "',claims_time_of_incident='" 
                          + csvData[i][30] + "',claims_delay_in_claim_information='" 
                          + csvData[i][31] + "',claims_pickup_company='" 
                          + csvData[i][32] + "',claims_pickup_street_area='" 
                          + csvData[i][33] + "',claims_unit='" 
                          + csvData[i][34] + "',claims_pick_up_city='" 
                          + csvData[i][35] + "',claims_pick_up_state='" 
                          + csvData[i][36] + "',claims_pin_code='" 
                          + csvData[i][37] + "',claims_pickup_country='" 
                          + csvData[i][38] + "',claims_dispatch_company='" 
                          + csvData[i][39] + "',claims_dispatch_street_area='" 
                          + csvData[i][40] + "',claims_dispatch_unit='" 
                          + csvData[i][41] + "',claims_dispatch_city='" 
                          + csvData[i][42] + "',claims_dispatch_state='" 
                          + csvData[i][43] + "',claims_dispatch_pincode='" 
                          + csvData[i][44] + "',claims_dispatch_country='" 
                          + csvData[i][45] + "',claims_excess_paid_date='" 
                          + csvData[i][46] + "',claims_excess_fee='" 
                          + csvData[i][47] + "',claims_excess_paid_by_customer='" 
                          + csvData[i][48] + "',claims_utr_cheque_number='" 
                          + csvData[i][49] + "',claims_excess_fee_actual='" 
                          + csvData[i][50] + "',claims_excess_fee_status='" 
                          + csvData[i][51] + "',claims_excess_fee_ber='" 
                          + csvData[i][52] + "',claims_excess_fee_ber_type='" 
                          + csvData[i][53] + "',claims_excess_fee_ber_status='" 
                          + csvData[i][54] + "',claims_excess_fee_ber_actual='" 
                          + csvData[i][55] + "',claims_policy_aging='"
                          + csvData[i][56] + "';";
                           conn.query(updateQtyQuery, (error, results, fields) => {
                          try {
                            res.json({
                                code: 200,
                                message: "File Uploaded Successfully",
                                status: true
                            });
                           }
                           catch(err) {
                               //console.log('Upload File:',err);
                           }
                         });
                        }}
                    });
                } else {
                    res.json({
                        status: false,
                        message: "Column(s) does not match."
                    });
                }
            });
        stream.pipe(csvStream);
    }
   
}

/*************End Upload file************/
