
var multer = require('multer');
var param=require('../shared/params');
var storage = multer.diskStorage({
	destination: (req, file, cb) => {
		//C:/xampp/htdocs/today/nodeapi/uploads/
		//console.log('***', __basedir + '/uploads/');
	    cb(null, param.UploadPath)
	},
	filename: (req, file, cb) => {
	   cb(null, file.fieldname + "-" + Date.now() + "-" + file.originalname)
	}
})

const upload = multer({storage: storage});

module.exports = upload;