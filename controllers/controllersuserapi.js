var { sequelize }            = require('../models');
var jwt                      = require('jsonwebtoken');
var bcrypt                   = require('bcryptjs');
var { User: usersModel,db }  = require('../models');
global.__basedir             = __dirname;

/*Get all value */

module.exports.getallUsers = function (req, res) {
    sequelize.query("select * from users", {
        type: sequelize.QueryTypes.SELECT
    }).then(users => {
        res.json({
             code: 200,
             status: true,
             response: users
        })
     }).catch(er => {
        res.json({
            status: er
        });
    });
}

module.exports.getallclaims = function (req, res) {
    sequelize.query("select * from claim_masters", {
        type: sequelize.QueryTypes.SELECT
    }).then(data => {
        res.json({
             code: 200,
             status: true,
             response: data
        })
     }).catch(er => {
        res.json({
            status: er
        });
    });
}




/*Get all value by Id */

module.exports.getclaimdetails = function (req, res) {
    sequelize.query("select * from claim_masters where claims_claims_number ='" + req.params.id + "'", {
        type: sequelize.QueryTypes.SELECT
    }).then(data => {
        if (data.length > 0) {
        res.json({
            code: 200,
            status: true,
            response: data
        });
    } else {
        res.json({
            status: false,
            message: "Record Not Found",
            response: null
        });
    }
    }).catch(er => {
        res.json({
            status: er
        });
    });
}

/*Login Authentication*/

module.exports.login = function (req, res) {
    usersModel.findOne({
        where: {
            username: req.body.username
        }
    }).then(user => {
        if (user) {
            if (bcrypt.compareSync(req.body.password, user.password)) {

                if (user.user_level == 1) {
                    var role = "Admin"
                    var userLevel=1;
                } else if (user.user_level == 2) {
                    var role = "Supervisor"
                    var userLevel=2;
                } else {
                    var role = "User"
                    var userLevel=3;
                }
                 const token = jwt.sign({
                        username: req.body.username,
                        password: req.body.password,
                        role:role,
                        userLevel:userLevel
                    },
                    'secretkey', {
                        expiresIn: "1h"
                    }
                );
                return res.status(200).json({
                    status: true,
                    code: 200,
                    response: {
                        token: token,
                        user: user
                    },
                })
            } else {
                res.json({
                    status: false,
                    message: "Invalid User Name/password!!!",
                    response: null
                });
            }
         } else {
            res.json({
                status: false,
                message: "You are not registered with us !",
                response: null
            });
        }
    })
};

/*Create User*/

module.exports.createUser = function (req, res) {

    let body = {
        "username"     : req.body.username,
        "user_email"   : req.body.user_email,
        "password"     : bcrypt.hashSync(req.body.password),
        "user_address" : req.body.user_address
    }
    usersModel.create(body).then(user => {
        res.json({
             code: 200,
             status: true,
             message: "Request was successfull"
        });
    }).catch(er => {
        res.json({
            status: err
        });
    });
}


/*Get user by ID*/

module.exports.getUsebyId = function (req, res) {
    usersModel.findAll({
        where: {
            'id': req.params.id
        }
    }).then(user => {
        if (user.length > 0) {
            res.json({
                code: 200,
                status: true,
                response: user
            });
        } else {
            res.json({
                status: false,
                message: "User Id Not found",
                response: null
            });
        }
    }).catch(er => {
        res.json({
            status: er
        });
    });
}

/*Delete user by ID*/

module.exports.deleteUsebyId = function (req, res) {

    usersModel.destroy({
        where: {
            'id': req.params.id
        }
    }).then(users => {
        console.log('users.length',users);
        if (users > 0) {
            res.json({
                code: 200,
                status: true,
                response: "Successfully deleted"
            });
        } else {
            res.json({
                message: "User Id Not found",
                response: null
            });
        }
    }).catch(er => {
        res.json({
            status: er
        });
    });
}

/*Update user by ID*/

module.exports.updateUsebyId = function (req, res) {

  let body = {
        "user_email": req.body.user_email
    }

    usersModel.update(body, {
        where: {
            'id': req.params.id
        }
    }).then(users => {
        if (users > 0) {
            res.json({
                code: 200,
                status: true,
                response: "Successfully updated"
            });
        } else {
            res.json({
                status: false,
                message: "You are not registered with us !",
                response: null
            });
        }

    }).catch(er => {
        res.json({
            status: er
        });
    });
}


function reverseDate(input){
    if(input != null || input !== undefined || input !== ''){
        var str = input.split('-');
        var value = str[2]+'-'+str[1]+'-'+str[0];
        return value;
    }
}

