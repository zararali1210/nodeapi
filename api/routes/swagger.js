/**
 * @swagger
 * /api/login:
 *   post:
 *     tags:
 *       - Auth
 *     name: Login
 *     summary: Logs in a user
 *     produces:
 *       - application/json
 *     consumes:
 *       - application/json
 *     parameters:
 *       - name: body
 *         in: body
 *         schema:
 *           type: object
 *           properties:
 *             username:
 *               type: string
 *             password:
 *               type: string
 *               format: password
 *         required:
 *           - username
 *           - password
 *     responses:
 *       '200':
 *         description: User found and logged in successfully
 *       '401':
 *         description: Bad username, not found in db
 *       '403':
 *         description: Username and password don't match
 */


 /**
 * @swagger
 * /api/createUser:
 *   post:
 *     security:
 *       - bearerAuth: []
 *     tags:
 *       - Users
 *     description: Creates a new User
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: body
 *         in: body
 *         schema:
 *           type: object
 *           properties:
 *             username:
 *               type: string
 *             user_email:
 *               type: string
 *             password:
 *               type: string
 *             user_address:
 *               type: string
 *         required:
 *           - username
 *           - password
 *     responses:
 *       200:
 *         description: Successfully created
 */

 /**
 * @swagger
 * /api/user/{id}:
 *   delete:
 *     security:
 *         - bearerAuth: []
 *     tags:
 *       - Users
 *     description: Deletes a single user
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: id
 *         description: User's Id
 *         in: path
 *         required: true
 *         type: integer
 *     responses:
 *       200:
 *         description: Successfully deleted
 */

 /**
 * @swagger
* /api/user/{id}:
 *   get:
 *     security:
 *       - bearerAuth: []
 *     tags:
 *       - Users
 *     description: Returns a single User
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: id
 *         description: User's Id
 *         in: path
 *         required: true
 *         type: integer
 *     responses:
 *       200:
 *         description: A User's Id
 */

/**
 * @swagger

 * /api/getallclaims:
 *  get:
 *    security:
 *       - bearerAuth: []
 *    tags:
 *       - Claims List
 *    description: Use to request all Users
 *    responses:
 *      '200':
 *        description: A successful response
 */

 /**
 * @swagger

 * /api/getallUsers:
 *  get:
 *    security:
 *       - bearerAuth: []
 *    tags:
 *       - Users
 *    description: Use to request all Users
 *    responses:
 *      '200':
 *        description: A successful response
 */

/**
 * @swagger
 * /api/edit/{id}:
 *    post:
 *      security:
 *         - bearerAuth: []
 *      tags:
 *        - Users
 *      description: Use to return all customers
 *      parameters:
 *        - name: id
 *          in: path
 *          description: User Id
 *          required: true
 *        - name: body
 *          in: body
 *          schema:
 *             type: object
 *             properties:
 *               user_email:
 *                 type: string
 *      responses:
 *         200:
 *          description: Successfully updated
 */

 
/**
 * @swagger
 * /api/uploadfile:
 *    post:
 *      security:
 *        - bearerAuth: []
 *      tags:
 *        - Upload File
 *      description: Use to return all customers
 *      parameters:
*      - in: formData
*        name: file
*        type: file
 *      responses:
 *         200:
 *          description: Successfully updated
 */

 
 /**
 * @swagger
* /api/getclaimdetails/{id}:
 *   get:
 *     security:
 *       - bearerAuth: []
 *     tags:
 *       - Claims List
 *     description: Returns a single Claim
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: id
 *         description: Claim's Number
 *         in: path
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Claim's Number
 */

 
  /**
 * @swagger

 * /api/getallarcData:
 *  get:
 *    security:
 *       - bearerAuth: []
 *    tags:
 *       - Claims List
 *    description: Use to request all List
 *    responses:
 *      '200':
 *        description: A successful response
 */

   /**
 * @swagger

 * /api/getallDispatchData:
 *  get:
 *    security:
 *       - bearerAuth: []
 *    tags:
 *       - Claims List
 *    description: Use to request all List
 *    responses:
 *      '200':
 *        description: A successful response
 */

    /**
 * @swagger

 * /api/getallPickupData:
 *  get:
 *    security:
 *       - bearerAuth: []
 *    tags:
 *       - Claims List
 *    description: Use to request all List
 *    responses:
 *      '200':
 *        description: A successful response
 */

 
    /**
 * @swagger

 * /api/getClaimStatus:
 *  get:
 *    security:
 *       - bearerAuth: []
 *    tags:
 *       - Claims List
 *    description: Use to request all List
 *    responses:
 *      '200':
 *        description: A successful response
 */

 /**
 * @swagger
* /api/getClaimSubStatus/{id}:
 *   get:
 *     security:
 *       - bearerAuth: []
 *     tags:
 *       - Claims List
 *     description: Returns a  Claim
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: id
 *         description: Claim's Status Id
 *         in: path
 *         required: true
 *         type: integer
 *     responses:
 *       200:
 *         description: Claim's Status Id
 */

 
/**
 * @swagger
 * /api/updateClaimMaster/{id}:
 *    post:
 *      security:
 *         - bearerAuth: []
 *      tags:
 *        - Claims List
 *      description: Update Data For Claim
 *      parameters:
 *        - name: id
 *          in: path
 *          description: Claim's Number
 *          required: true
 *        - name: body
 *          in: body
 *          schema:
 *             type: object
 *             properties:
 *               claims_claims_number:
 *                 type: string
 *               pickup:
 *                 type: string
 *               dispatch:
 *                 type: string
 *               arc:
 *                 type: string
 *               claim_status:
 *                 type: string
 *               claim_sub_status:
 *                  type: string
 *               updated_by:
 *                  type: number
 *               comments:
 *                  type: string
 *      responses:
 *         200:
 *          description: Successfully updated
 */


 
// /**
//  * @swagger
//  * /api/createClaimHistory:
//  *    post:
//  *      security:
//  *         - bearerAuth: []
//  *      tags:
//  *        - Claims List
//  *      description: Update Data For Claim
//  *      parameters:
//  *        - name: body
//  *          in: body
//  *          schema:
//  *             type: object
//  *             properties:
//  *               claims_claims_number:
//  *                 type: string
//  *               pickup:
//  *                 type: string
//  *               dispatch:
//  *                 type: string
//  *               arc:
//  *                 type: string
//  *               claim_status:
//  *                 type: string
//  *               claim_sub_status:
//  *                  type: string
//  *               updated_by:
//  *                  type: number
//  *               comments:
//  *                  type: string
//  *      responses:
//  *         200:
//  *          description: Successfully updated
//  */

 