"use strict";

module.exports = function (sequelize, DataTypes) {
  var ClaimHistory = sequelize.define("ClaimHistory", {
    claims_claims_number: {
      type: DataTypes.STRING(100),
      validate: {}
    },
    pickup: {
      type: DataTypes.STRING(200),
      validate: {}
    },
    dispatch: {
      type: DataTypes.STRING(200),
      validate: {}
    },
    arc: {
      type: DataTypes.STRING(200),
      validate: {}
    },
    claim_status: {
      type: DataTypes.STRING(200),
      validate: {}
    },
    claim_sub_status: {
      type: DataTypes.STRING(100),
      validate: {}
    },
    comments: {
        type: DataTypes.STRING(200),
        validate: {}
      },
    updated_by: {
      type: DataTypes.INTEGER,
      validate: {}
    },
    is_deleted: DataTypes.BOOLEAN
  }, {

    // don't use camelcase for automatically added attributes but underscore style
    // so updatedAt will be updated_at
    underscored: true,

    // don't add the timestamp attributes (updatedAt, createdAt)
    timestamps: true,

    // don't delete database entries but set the newly added attribute deletedAt
    // to the current date (when deletion was done). paranoid will only work if
    // timestamps are enabled
    paranoid: true

  });
  ClaimHistory.associate = function (models) {}
  return ClaimHistory;
};