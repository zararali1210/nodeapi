"use strict";

module.exports = function (sequelize, DataTypes) {
  var Dispatch = sequelize.define("Dispatch", {
    name: {
      type: DataTypes.STRING(100),
      validate: {}
    },
    address: {
      type: DataTypes.STRING(200),
      validate: {}
    },
    number: {
      type: DataTypes.INTEGER(20),

      validate: {}
    },
    isDeleted: DataTypes.BOOLEAN
  }, {

    // don't use camelcase for automatically added attributes but underscore style
    // so updatedAt will be updated_at
    underscored: true,

    // don't add the timestamp attributes (updatedAt, createdAt)
    timestamps: true,

    // don't delete database entries but set the newly added attribute deletedAt
    // to the current date (when deletion was done). paranoid will only work if
    // timestamps are enabled
    paranoid: true

  });
  Dispatch.associate = function (models) {}
  return Dispatch;
};