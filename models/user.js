"use strict";

module.exports = function (sequelize, DataTypes) {
  var User = sequelize.define("User", {
    username: {
      type: DataTypes.STRING(50),
      validate: {}
    },
    password: {
      type: DataTypes.STRING,
      allowNull: true,
      unique: false,
      validate: {}
    },
    user_type: {
      type: DataTypes.INTEGER,

      validate: {}
    },
    user_level: {
      type: DataTypes.INTEGER,
      validate: {}
    },
    user_country: {
      type: DataTypes.STRING(50),
      validate: {}
    },
    user_pincode: {
      type: DataTypes.INTEGER,
      validate: {}
    },
    user_state: {
      type: DataTypes.STRING(50),
      validate: {}
    },
    isActive: {
      type: DataTypes.INTEGER,
      validate: {}
    },
    user_contact: {
      type: DataTypes.INTEGER,
      validate: {}
    },
    user_email: {
      type: DataTypes.STRING,
      unique: true,
      validate: {
        isEmail: true,
      }
    },
    user_address: {
      type: DataTypes.STRING,
      validate: {}
    },
    resetPasswordToken: {

      type: DataTypes.STRING,
      validate: {}

    },
    resetPasswordExpires: {

      type: DataTypes.DATE,
      validate: {}

    },
    isDeleted: DataTypes.BOOLEAN
  }, {

    // don't use camelcase for automatically added attributes but underscore style
    // so updatedAt will be updated_at
    underscored: true,

    // don't add the timestamp attributes (updatedAt, createdAt)
    timestamps: true,

    // don't delete database entries but set the newly added attribute deletedAt
    // to the current date (when deletion was done). paranoid will only work if
    // timestamps are enabled
    paranoid: true

  });
  User.associate = function (models) {}
  return User;
};