"use strict";

module.exports = function (sequelize, DataTypes) {
  var ClaimSubstatus = sequelize.define("ClaimSubstatus", {
    claimSubstatus: {
      type: DataTypes.STRING(100),
      validate: {}
    },
    claimStatusId: {
        type: DataTypes.INTEGER,
        validate: {}
      },
    isDeleted: DataTypes.BOOLEAN
  }, {

    // don't use camelcase for automatically added attributes but underscore style
    // so updatedAt will be updated_at
    underscored: true,

    // don't add the timestamp attributes (updatedAt, createdAt)
    timestamps: true,

    // don't delete database entries but set the newly added attribute deletedAt
    // to the current date (when deletion was done). paranoid will only work if
    // timestamps are enabled
    paranoid: true

  });
  ClaimSubstatus.associate = function (models) {}
  return ClaimSubstatus;
};