module.exports = function (sequelize, DataTypes) {
    var ClaimMaster = sequelize.define("ClaimMaster", {
        claims_claims_number: {
            type: DataTypes.STRING(100),
            validate: {}
        },

        claims_imei: {
            type: DataTypes.STRING(100),
            validate: {}
        },

        claims_policy_start_date: {
            type: DataTypes.STRING(100),
            validate: {}
        },

        claims_policy_end_date: {
            type: DataTypes.STRING(100),
            validate: {}
        },

        claims_mobile_purchase_date: {
            type: DataTypes.STRING(100),
            validate: {}
        },

        claims_store_name: {
            type: DataTypes.STRING(100),
            validate: {}
        },

        claims_policy_type: {
            type: DataTypes.STRING(100),
            validate: {}
        },

        claims_master_policy_number: {
            type: DataTypes.STRING(100),
            validate: {}
        },

        claims_certificate_of_insurance: {
            type: DataTypes.STRING(100),
            validate: {}
        },

        claims_make: {
            type: DataTypes.STRING(100),
            validate: {}
        },

        claims_model: {
            type: DataTypes.STRING(200),
            validate: {}
        },

        claims_cost_mobile: {
            type: DataTypes.STRING(100),
            validate: {}
        },

        claims_policy_status: {
            type: DataTypes.STRING(50),
            validate: {}
        },

        claims_created_time: {
            type: DataTypes.STRING(100),
            validate: {}
        },

        claims_modified_time: {
            type: DataTypes.STRING(100),
            validate: {}
        },

        claims_last_modified_by: {
            type: DataTypes.STRING(100),
            validate: {}
        },

        claims_client_name: {
            type: DataTypes.STRING(100),
            validate: {}
        },

        claims_customer_name: {
            type: DataTypes.STRING(100),
            validate: {}
        },

        claims_home_contact: {
            type: DataTypes.INTEGER(50),
            validate: {}
        },

        claims_email: {
            type: DataTypes.STRING(200),
            validate: {}
        },

        claims_alternative_landline_number: {
            type: DataTypes.STRING(100),
            validate: {}
        },

        claims_subject: {
            type: DataTypes.STRING(200),
            validate: {}
        },

        claims_claim_date: {
            type: DataTypes.STRING(100),
            validate: {}
        },

        claims_claim_type: {
            type: DataTypes.STRING(100),
            validate: {}
        },

        claims_assigned_to: {
            type: DataTypes.STRING(100),
            validate: {}
        },

        claims_claim_cause: {
            type: DataTypes.STRING(200),
            validate: {}
        },

        claims_coverage_type: {
            type: DataTypes.STRING(100),
            validate: {}
        },

        claims_contact_number: {
            type: DataTypes.STRING(100),
            validate: {}
        },

        claims_date_of_incident: {
            type: DataTypes.STRING(100),
            validate: {}
        },

        claims_where_did_it_happen: {
            type: DataTypes.TEXT,
            validate: {}
        },

        claims_time_of_incident: {
            type: DataTypes.STRING(100),
            validate: {}
        },
        
        claims_delay_in_claim_information: {
            type: DataTypes.TEXT,
            validate: {}
        },

        claims_pickup_company: {
            type: DataTypes.STRING(200),
            validate: {}
        },

        claims_pickup_street_area: {
            type: DataTypes.TEXT,
            validate: {}
        },

        claims_unit: {
            type: DataTypes.TEXT,
            validate: {}
        },

        claims_pick_up_city: {
            type: DataTypes.STRING(100),
            validate: {}
        },

        claims_pick_up_state: {
            type: DataTypes.STRING(100),
            validate: {}
        },

        claims_pin_code: {
            type: DataTypes.STRING(50),
            validate: {}
        },

        claims_pickup_country: {
            type: DataTypes.STRING(100),
            validate: {}
        },

        claims_dispatch_company: {
            type: DataTypes.STRING(200),
            validate: {}
        },

        claims_dispatch_street_area: {
            type: DataTypes.STRING(400),
            validate: {}
        },

        claims_dispatch_unit: {
            type: DataTypes.STRING(400),
            validate: {}
        },

        claims_dispatch_city: {
            type: DataTypes.STRING(100),
            validate: {}
        },

        claims_dispatch_state: {
            type: DataTypes.STRING(100),
            validate: {}
        },

        claims_dispatch_pincode: {
            type: DataTypes.STRING(50),
            validate: {}
        },

        claims_dispatch_country: {
            type: DataTypes.STRING(100),
            validate: {}
        },

        claims_excess_paid_date: {
            type: DataTypes.STRING(100),
            validate: {}
        },

        claims_excess_fee: {
            type: DataTypes.STRING(200),
            validate: {}
        },

        claims_excess_paid_by_customer: {
            type: DataTypes.STRING(100),
            validate: {}
        },

        claims_utr_cheque_number: {
            type: DataTypes.STRING(100),
            validate: {}
        },

        claims_excess_fee_actual: {
            type: DataTypes.STRING(200),
            validate: {}
        },

        claims_excess_fee_status: {
            type: DataTypes.STRING(400),
            validate: {}
        },

        claims_excess_fee_ber: {
            type: DataTypes.STRING(100),
            validate: {}
        },

        claims_excess_fee_ber_type: {
            type: DataTypes.STRING(100),
            validate: {}
        },

        claims_excess_fee_ber_status: {
            type: DataTypes.STRING(200),
            validate: {}
        },

        claims_excess_fee_ber_actual: {
            type: DataTypes.STRING(100),
            validate: {}
        },

        claims_policy_aging: {
            type: DataTypes.STRING(100),
            validate: {}
        },

        pickup: {
            type: DataTypes.STRING(100),
            validate: {}
        },

        dispatch: {
            type: DataTypes.STRING(200),
            validate: {}
        },

        arc: {
            type: DataTypes.STRING(200),
            validate: {}
        },

        claim_status: {
            type: DataTypes.STRING(200),
            validate: {}
        },

        claim_sub_status: {
            type: DataTypes.STRING(100),
            validate: {}
        },
        updated_by: {
            type: DataTypes.INTEGER,
            validate: {}
        },
        comments: {
            type: DataTypes.STRING(100),
            validate: {}
        },
        isDeleted: DataTypes.BOOLEAN
    }, {

        // don't use camelcase for automatically added attributes but underscore style
        // so updatedAt will be updated_at
        underscored: true,

        // don't add the timestamp attributes (updatedAt, createdAt)
        timestamps: true,

        // don't delete database entries but set the newly added attribute deletedAt
        // to the current date (when deletion was done). paranoid will only work if
        // timestamps are enabled
        paranoid: true

    });
    ClaimMaster.associate = function (models) {}
    return ClaimMaster;
};