//require('dotenv').config()
var express              = require('express');
var bodyParser           = require('body-parser');
var cors                 = require('cors');
var { sequelize }        = require('./models');
var app                  = express();
var swaggerJsDoc         = require("swagger-jsdoc");
var swaggerUi            = require("swagger-ui-express");
var swaggerDefinition    = require('./api/routes/swaggerDoc');

global.__basedir         = __dirname;
app.use(bodyParser.json())
app.use(cors());


var optionsroute = {
  swaggerDefinition,
  apis: ['./api/routes/*.js'],
};

var swaggerDocs = swaggerJsDoc(optionsroute);
app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocs));

app.use(bodyParser.urlencoded({
  extended: false
}))

app.use('/', require('./routes/routesuserapi'));
app.use('/', require('./routes/routeclaimsapi'));

sequelize.sync().then(function () {
  app.listen(2000, function () {
    console.log('Express server listening on port 2000 and connected to database');
  });
});